<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferTotalOfferPivotTable extends Migration
{
    public function up()
    {
        Schema::create('offer_total_offer', function (Blueprint $table) {
            $table->unsignedBigInteger('total_offer_id');
            $table->foreign('total_offer_id', 'total_offer_id_fk_4749348')->references('id')->on('total_offers')->onDelete('cascade');
            $table->unsignedBigInteger('offer_id');
            $table->foreign('offer_id', 'offer_id_fk_4749348')->references('id')->on('offers')->onDelete('cascade');
        });
    }
}
