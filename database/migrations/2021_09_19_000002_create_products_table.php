<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->longText('flower_care')->nullable();
            $table->string('size')->nullable();
            $table->integer('height')->nullable();
            $table->float('price', 10, 2);
            $table->float('discount_price', 10, 2)->nullable();
            $table->integer('in_stock')->nullable();
            $table->boolean('in_constructor')->default(0)->nullable();
            $table->boolean('bestseller')->default(0)->nullable();
            $table->boolean('sale')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
