<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTotalOffersTable extends Migration
{
    public function up()
    {
        Schema::create('total_offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->integer('number')->nullable();
            $table->integer('summ')->nullable();
            $table->boolean('is_constructor')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
