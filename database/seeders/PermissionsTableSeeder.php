<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'shop_access',
            ],
            [
                'id'    => 18,
                'title' => 'category_create',
            ],
            [
                'id'    => 19,
                'title' => 'category_edit',
            ],
            [
                'id'    => 20,
                'title' => 'category_show',
            ],
            [
                'id'    => 21,
                'title' => 'category_delete',
            ],
            [
                'id'    => 22,
                'title' => 'category_access',
            ],
            [
                'id'    => 23,
                'title' => 'event_create',
            ],
            [
                'id'    => 24,
                'title' => 'event_edit',
            ],
            [
                'id'    => 25,
                'title' => 'event_show',
            ],
            [
                'id'    => 26,
                'title' => 'event_delete',
            ],
            [
                'id'    => 27,
                'title' => 'event_access',
            ],
            [
                'id'    => 28,
                'title' => 'color_create',
            ],
            [
                'id'    => 29,
                'title' => 'color_edit',
            ],
            [
                'id'    => 30,
                'title' => 'color_show',
            ],
            [
                'id'    => 31,
                'title' => 'color_delete',
            ],
            [
                'id'    => 32,
                'title' => 'color_access',
            ],
            [
                'id'    => 33,
                'title' => 'product_create',
            ],
            [
                'id'    => 34,
                'title' => 'product_edit',
            ],
            [
                'id'    => 35,
                'title' => 'product_show',
            ],
            [
                'id'    => 36,
                'title' => 'product_delete',
            ],
            [
                'id'    => 37,
                'title' => 'product_access',
            ],
            [
                'id'    => 38,
                'title' => 'tag_create',
            ],
            [
                'id'    => 39,
                'title' => 'tag_edit',
            ],
            [
                'id'    => 40,
                'title' => 'tag_show',
            ],
            [
                'id'    => 41,
                'title' => 'tag_delete',
            ],
            [
                'id'    => 42,
                'title' => 'tag_access',
            ],
            [
                'id'    => 43,
                'title' => 'promo_code_create',
            ],
            [
                'id'    => 44,
                'title' => 'promo_code_edit',
            ],
            [
                'id'    => 45,
                'title' => 'promo_code_show',
            ],
            [
                'id'    => 46,
                'title' => 'promo_code_delete',
            ],
            [
                'id'    => 47,
                'title' => 'promo_code_access',
            ],
            [
                'id'    => 48,
                'title' => 'address_create',
            ],
            [
                'id'    => 49,
                'title' => 'address_edit',
            ],
            [
                'id'    => 50,
                'title' => 'address_show',
            ],
            [
                'id'    => 51,
                'title' => 'address_delete',
            ],
            [
                'id'    => 52,
                'title' => 'address_access',
            ],
            [
                'id'    => 53,
                'title' => 'delivery_period_create',
            ],
            [
                'id'    => 54,
                'title' => 'delivery_period_edit',
            ],
            [
                'id'    => 55,
                'title' => 'delivery_period_show',
            ],
            [
                'id'    => 56,
                'title' => 'delivery_period_delete',
            ],
            [
                'id'    => 57,
                'title' => 'delivery_period_access',
            ],
            [
                'id'    => 58,
                'title' => 'bonu_create',
            ],
            [
                'id'    => 59,
                'title' => 'bonu_edit',
            ],
            [
                'id'    => 60,
                'title' => 'bonu_show',
            ],
            [
                'id'    => 61,
                'title' => 'bonu_delete',
            ],
            [
                'id'    => 62,
                'title' => 'bonu_access',
            ],
            [
                'id'    => 63,
                'title' => 'offer_create',
            ],
            [
                'id'    => 64,
                'title' => 'offer_edit',
            ],
            [
                'id'    => 65,
                'title' => 'offer_show',
            ],
            [
                'id'    => 66,
                'title' => 'offer_delete',
            ],
            [
                'id'    => 67,
                'title' => 'offer_access',
            ],
            [
                'id'    => 68,
                'title' => 'total_offer_create',
            ],
            [
                'id'    => 69,
                'title' => 'total_offer_edit',
            ],
            [
                'id'    => 70,
                'title' => 'total_offer_show',
            ],
            [
                'id'    => 71,
                'title' => 'total_offer_delete',
            ],
            [
                'id'    => 72,
                'title' => 'total_offer_access',
            ],
            [
                'id'    => 73,
                'title' => 'user_event_create',
            ],
            [
                'id'    => 74,
                'title' => 'user_event_edit',
            ],
            [
                'id'    => 75,
                'title' => 'user_event_show',
            ],
            [
                'id'    => 76,
                'title' => 'user_event_delete',
            ],
            [
                'id'    => 77,
                'title' => 'user_event_access',
            ],
            [
                'id'    => 78,
                'title' => 'setting_create',
            ],
            [
                'id'    => 79,
                'title' => 'setting_edit',
            ],
            [
                'id'    => 80,
                'title' => 'setting_show',
            ],
            [
                'id'    => 81,
                'title' => 'setting_delete',
            ],
            [
                'id'    => 82,
                'title' => 'setting_access',
            ],
            [
                'id'    => 83,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
