<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTotalOfferRequest;
use App\Http\Requests\UpdateTotalOfferRequest;
use App\Http\Resources\Admin\TotalOfferResource;
use App\Models\TotalOffer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TotalOfferApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('total_offer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TotalOfferResource(TotalOffer::with(['offers'])->get());
    }

    public function store(StoreTotalOfferRequest $request)
    {
        $totalOffer = TotalOffer::create($request->all());
        $totalOffer->offers()->sync($request->input('offers', []));

        return (new TotalOfferResource($totalOffer))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(TotalOffer $totalOffer)
    {
        abort_if(Gate::denies('total_offer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new TotalOfferResource($totalOffer->load(['offers']));
    }

    public function update(UpdateTotalOfferRequest $request, TotalOffer $totalOffer)
    {
        $totalOffer->update($request->all());
        $totalOffer->offers()->sync($request->input('offers', []));

        return (new TotalOfferResource($totalOffer))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(TotalOffer $totalOffer)
    {
        abort_if(Gate::denies('total_offer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $totalOffer->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
