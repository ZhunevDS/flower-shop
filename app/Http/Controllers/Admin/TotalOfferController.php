<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyTotalOfferRequest;
use App\Http\Requests\StoreTotalOfferRequest;
use App\Http\Requests\UpdateTotalOfferRequest;
use App\Models\Offer;
use App\Models\TotalOffer;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TotalOfferController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('total_offer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $totalOffers = TotalOffer::with(['offers'])->get();

        return view('admin.totalOffers.index', compact('totalOffers'));
    }

    public function create()
    {
        abort_if(Gate::denies('total_offer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $offers = Offer::pluck('name', 'id');

        return view('admin.totalOffers.create', compact('offers'));
    }

    public function store(StoreTotalOfferRequest $request)
    {
        $totalOffer = TotalOffer::create($request->all());
        $totalOffer->offers()->sync($request->input('offers', []));

        return redirect()->route('admin.total-offers.index');
    }

    public function edit(TotalOffer $totalOffer)
    {
        abort_if(Gate::denies('total_offer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $offers = Offer::pluck('name', 'id');

        $totalOffer->load('offers');

        return view('admin.totalOffers.edit', compact('offers', 'totalOffer'));
    }

    public function update(UpdateTotalOfferRequest $request, TotalOffer $totalOffer)
    {
        $totalOffer->update($request->all());
        $totalOffer->offers()->sync($request->input('offers', []));

        return redirect()->route('admin.total-offers.index');
    }

    public function show(TotalOffer $totalOffer)
    {
        abort_if(Gate::denies('total_offer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $totalOffer->load('offers');

        return view('admin.totalOffers.show', compact('totalOffer'));
    }

    public function destroy(TotalOffer $totalOffer)
    {
        abort_if(Gate::denies('total_offer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $totalOffer->delete();

        return back();
    }

    public function massDestroy(MassDestroyTotalOfferRequest $request)
    {
        TotalOffer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
