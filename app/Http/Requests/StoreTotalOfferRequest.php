<?php

namespace App\Http\Requests;

use App\Models\TotalOffer;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreTotalOfferRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('total_offer_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'nullable',
            ],
            'offers.*' => [
                'integer',
            ],
            'offers' => [
                'required',
                'array',
            ],
            'number' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'summ' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
            'is_constructor' => [
                'required',
            ],
        ];
    }
}
