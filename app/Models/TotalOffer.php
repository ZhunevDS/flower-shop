<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TotalOffer extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'total_offers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'number',
        'summ',
        'is_constructor',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
