<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::post('users/media', 'UsersApiController@storeMedia')->name('users.storeMedia');
    Route::apiResource('users', 'UsersApiController');

    // Category
    Route::post('categories/media', 'CategoryApiController@storeMedia')->name('categories.storeMedia');
    Route::apiResource('categories', 'CategoryApiController');

    // Event
    Route::apiResource('events', 'EventApiController');

    // Color
    Route::apiResource('colors', 'ColorApiController');

    // Product
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('products', 'ProductApiController');

    // Promo Code
    Route::apiResource('promo-codes', 'PromoCodeApiController');

    // Address
    Route::apiResource('addresses', 'AddressApiController');

    // Delivery Period
    Route::apiResource('delivery-periods', 'DeliveryPeriodApiController');

    // Bonus
    Route::apiResource('bonus', 'BonusApiController');

    // Offer
    Route::apiResource('offers', 'OfferApiController');

    // Total Offer
    Route::apiResource('total-offers', 'TotalOfferApiController');

    // User Event
    Route::apiResource('user-events', 'UserEventApiController');

    // Setting
    Route::post('settings/media', 'SettingApiController@storeMedia')->name('settings.storeMedia');
    Route::apiResource('settings', 'SettingApiController');
});
