@extends('layouts.frontend')
@section('content')
<main class="main-page">
    <section class="section home">
        <div class='container home__container'>

            <div class="home-body">
                <div class="col home-col home-col__left">
                    <p class="subtitle home__subtitle">Твой букет говорит за тебя</p>
                    <h1 class="title home__title">Цветы и подарки <span>в Зеленограде</span></h1>
                    <a href="catalog.html" class="btn home__btn">Перейти к каталогу</a>
                </div>
                <div class="col home-col  home-col__right">
                    <img class="home__img" src="img/home/flower.png" alt="flower"/>
                </div>
                <div class="col home-col home-col__select">
                    <form action="#" class="form select-form">
                        <div class="select-form__inner">

                            <div class="select-form__head">
                                <h3 class="select-form__title">Подобрать букет</h3>
                                <button type="submit" class="btn select-form__btn"
                                        data-da="select-form__inner,5,767.98">Показать
                                </button>
                            </div>

                            <div class="select-form__body">
                                <div class="select-form__group">
                                    <select name="form[]" class="form-block icon-arrow-down">
                                        <option value="1" selected="selected">Букеты и подарки</option>
                                        <option value="2">Наименование</option>
                                        <option value="3">Наименование</option>
                                        <option value="4">Наименование</option>
                                        <option value="5">Наименование</option>
                                        <option value="6">Наименование</option>

                                    </select>
                                </div>
                                <div class="select-form__group">
                                    <select name="form[]" class="form-block">
                                        <option value="1" selected="selected">Цветы</option>
                                        <option value="2">Розы</option>
                                        <option value="3">Фиалки</option>
                                        <option value="4">Наименование</option>
                                        <option value="5">Наименование</option>
                                        <option value="6">Наименование</option>
                                    </select>
                                </div>
                                <div class="select-form__group">
                                    <select name="form[]" class="form-block">
                                        <option value="1" selected="selected">Цвет</option>
                                        <option value="2">Красный</option>
                                        <option value="3">Зеленный</option>
                                        <option value="4">Жолтый</option>
                                        <option value="5">Синий</option>
                                        <option value="6">Жолтый</option>
                                    </select>
                                </div>
                                <div class="select-form__group">
                                    <select name="form[]" class="form-block">
                                        <option value="1" selected="selected">Повод</option>
                                        <option value="2">День рождение</option>
                                        <option value="3">Свадьба</option>
                                        <option value="4">Юбилей</option>
                                        <option value="5">Свадьба</option>
                                        <option value="6">Свадьба</option>
                                    </select>
                                </div>
                                <div class="select-form__group select-form__group--filters">
                                    <div class="select-form__group--block ">
                                        <input class="select-form__group--input" placeholder="Цена"></input>
                                        <span class="icon-arrow-down filters__btn"></span>
                                    </div>
                                    <div class="search-filters filters">
                                        <div class="filters__item filters-price">
                                            <h3 class="filters-price__title">Ценовой диапазон</h3>
                                            <div class="filters-price__inputs">
                                                <label class="filters-price__label">
                                                    <input type="number" min="0" max="75000" placeholder="0"
                                                           class="filters-price__input" id="input-0">
                                                    <span class="filters-price__text">₽</span>
                                                </label>
                                                <label class="filters-price__label">
                                                    <span class="filters-price__text filters-price__text-rang">-</span>
                                                    <input type="number" min="0" max="75000" placeholder="75000"
                                                           class="filters-price__input" id="input-1">
                                                    <span class="filters-price__text">₽</span>
                                                </label>
                                            </div>
                                            <div class="filters-price__slider" id="range-slider">
                                                <svg viewBox="0 0 315 77">
                                                    <path
                                                        d="M166.5 76.5001C168.833 71.0001 175.2 60.5001 182 62.5001C188.8 64.5001 192.833 72.6668 194 76.5001H196.967C198.744 75.9345 200.839 74.2962 202.5 70.5001C206 62.5001 208 50.5001 211.5 52.5001C215 54.5001 213 71.0001 216.5 65.5001C220 60.0001 217.5 43.0001 221.5 44.0001C225.5 45.0001 223 60.5 226 65.5001C229 70.5001 227.5 24.5 230.5 24.5C233.5 24.5 237 22.5 238.5 31C240 39.5 249 31.9999 249 24.5C249 17 250.5 -2 254.5 1C258.5 4 255 27.5 258 37.5C261 47.5 256.5 76.5001 306.5 76.5001H196.967C195.748 76.8881 194.678 76.7714 194 76.5001H166.5Z"
                                                        fill="#E4DFF0"/>
                                                    <path
                                                        d="M175 76.8461C172.244 73.4478 164.724 66.9601 156.693 68.1959C148.661 69.4316 143.897 74.4776 142.519 76.8461H139.015C136.916 76.4966 134.441 75.4844 132.48 73.1388C128.346 68.1959 125.984 60.7814 121.85 62.0171C117.716 63.2529 120.078 73.4478 115.944 70.0495C111.81 66.6512 114.763 56.1473 110.038 56.7652C105.314 57.3831 108.267 66.9601 104.723 70.0495C101.18 73.1388 102.952 44.7167 99.4084 44.7167C95.865 44.7167 91.7311 43.4809 89.9594 48.7328C88.1878 53.9847 77.5577 49.3507 77.5577 44.7167C77.5577 40.0827 75.786 28.3431 71.0616 30.1967C66.3371 32.0503 70.471 46.5703 66.9276 52.749C63.3843 58.9277 68.6993 76.8461 9.64337 76.8461H139.015C140.455 77.0858 141.718 77.0137 142.519 76.8461H175Z"
                                                        fill="#E4DFF0"/>
                                                </svg>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                    <div class="home__decor home__decor-4"></div>
                </div>
            </div>
            <div class="home__decor home__decor-1"></div>
            <div class="home__decor home__decor-2"></div>
            <div class="home__decor home__decor-3"></div>
        </div>
    </section>
    <div class="section category">
        <div class="container category__container">
            <div class="category__slider slider">
                <div class="swiper-container category__slider-container">
                    <div class="swiper-wrapper slider__wrapper">

                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/1.png" alt="catalog-img"/>
                                </div>
                            </div>

                            <a href="" class="slider__link">
                                <h5 class="slider__title">Акции</h5>
                            </a>

                        </div>
                        <div class="swiper-slide slider__item">

                            <div class="slider__item-img">
                                <div class="slider__item-img-box top35">
                                    <img src="img/catalog/2.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Монобукеты</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box top35">
                                    <img src="img/catalog/3.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Авторские <br>
                                    букеты</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/4.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Композиции</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/5.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Корзины цветов</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/6.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Подарки</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/7.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Фруктовые букеты</h5>
                            </a>
                        </div>
                        <div class="swiper-slide slider__item">
                            <div class="slider__item-img">
                                <div class="slider__item-img-box">
                                    <img src="img/catalog/8.png" alt="catalog-img"/>
                                </div>
                            </div>
                            <a href="" class="slider__link">

                                <h5 class="slider__title">Собери сам</h5>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="swiper-navigation">
                    <button class="slider__button-prev btn-action icon-arrow-down"></button>
                    <button class="slider__button-next btn-action icon-arrow-down"></button>
                </div>
            </div>
        </div>
    </div>
    <div class="search">
        <div class='container'>
            <div class="search__form form">
                <form action="#" class="search__form-item">

                    <div class="search__form-item-input">
                        <button class="search__form-icon icon-serch"></button>
                        <input autocomplete="off" type="text" placeholder="Поиск" class="search__form-input">
                    </div>

                </form>
            </div>
        </div>
    </div>
    <section class="section product">
        <div class='container product__container'>
            <div class="section__head">
                <h2 class="section__title">Хиты продаж</h2>
                <div class="section__action">
                    <span>Все xиты продаж</span>
                    <a href="catalog.html" class="btn-action icon-arrow-down"></a>
                </div>
            </div>


            <div class="product__body">
                <div class="product__hits-container">
                    <div class="product__wrapper">

                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn" data-path="first"
                                       data-animation="fadeInUp"
                                       data-speed="500">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="section product">
        <div class='container product__container'>
            <div class="section__head">
                <h2 class="section__title">Авторские букеты</h2>
                <div class="section__action">
                    <span>Все авторские букеты</span>
                    <a href="catalog.html" class="btn-action icon-arrow-down"></a>
                </div>
            </div>


            <div class="product__body">
                <div class="product__author-container">
                    <div class="product__wrapper">

                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-author/product-author-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-author/product-author-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-author/product-author-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-author/product-author-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-author/product-author-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="section product">
        <div class='container product__container'>
            <div class="section__head">
                <h2 class="section__title">Композиции</h2>
                <div class="section__action">
                    <span>Все композиции</span>
                    <a href="catalog.html" class="btn-action icon-arrow-down"></a>
                </div>
            </div>


            <div class="product__body">
                <div class="product__composition-container">
                    <div class="product__wrapper">

                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-hits/product-img-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
    <section class="section product">
        <div class='container product__container'>
            <div class="section__head">
                <h2 class="section__title">Подарки</h2>
                <div class="section__action">
                    <span>Все подарки</span>
                    <a href="catalog.html" class="btn-action icon-arrow-down"></a>
                </div>
            </div>


            <div class="product__body">
                <div class="product__presents-container">
                    <div class="product__wrapper">

                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-presents/product-presents-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart">
                                        <span class="action__cart-count cart-count">1</span>
                                    </a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-presents/product-presents-2.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-presents/product-presents-3.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-presents/product-presents-4.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>
                        <div class="product__item">
                            <div class="product__item-img">
                                <img src="img/product-presents/product-presents-1.jpg" alt="product-img"/>
                            </div>
                            <div class="product__item-body">
                                <div class="product__item-box">
                                    <a href="card.html">
                                        <h4 class="product__title">Название букета</h4>
                                    </a>
                                </div>
                                <div class="product__item-box">
                                    <div class="product__item-box--left">
                                        <span class="product__item-text">К-во букетов:</span>
                                        <span class="product__price">2500 Р</span>
                                    </div>
                                    <div class="product__count">
                                        <span class="product__count-icon icon-minus"></span>
                                        <span class="product__count-icon product__count-counter">1</span>
                                        <span class="product__count-icon icon-plus"></span>
                                    </div>
                                </div>
                                <div class="product__item-box">
                                    <a href="#" class="product__cart icon-cart"></a>
                                    <a href="#" class="btn btn-white product__item-btn">Быстрый заказ</a>
                                </div>
                            </div>
                            <div class="product__favorite icon-favorites favorite"></div>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </section>
</main>
@endsection
