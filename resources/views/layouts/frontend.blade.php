<!DOCTYPE html>
<html lang="ru">

<head>
    <title>Цветы и подарки в Зеленограде</title>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/swiper.min.css">
    <link rel="shortcut icon" href="favicon.ico">

    @yield('styles')
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
</head>

<body class="main-body">
<div class="wrapper">
    <header id="header" class="header">
        <div class="header__inner">
            <div class="header__left d-flex">
                <a href="/" class="header__logo logo">
                    <img src="img/logo.svg" alt="logo">
                </a>

                <div class="header__location location icon-location" data-da="header-menu__list,7,790">
                    Выберите город
                </div>
            </div>

            <div class="header__right d-flex">


                <div class="header__phone phone" data-da="header-menu__list,4,1200">
                    <a href="tel:+79999999999">+7 999 999-99-99</a>
                </div>
                <div class="header-menu__icon">
                    <span></span>
                </div>
                <div class="header__social social" data-da="header-menu__list,5,968">
                    <ul class="social__list">
                        <li class="social__item"><a href="#" class="social__link icon-telegram"></a></li>
                        <li class="social__item"><a href="#" class="social__link icon-whatsapp"></a></li>
                        <li class="social__item"><a href="#" class="social__link icon-inst"></a></li>
                    </ul>
                </div>
                <nav id="header-menu" class="header-menu">
                    <ul class="header-menu__list">
                        <li class="header-menu__item"><a href="" class="header-menu__link">Навигационная ссылка</a></li>
                        <li class="header-menu__item"><a href="" class="header-menu__link">Навигационная ссылка</a></li>
                        <li class="header-menu__item"><a href="" class="header-menu__link">Навигационная ссылка</a></li>
                    </ul>
                </nav>
                <div class="header__action action" data-da="header-menu__list,6,600">
                    <div class="action__user icon-avatar"></div>
                    <a href="checkout.html" class="action__cart icon-cart"><span
                            class="action__cart-count cart-count">1</span></a>
                </div>
            </div>
        </div>
    </header>

    @yield("content")


    <footer class="footer">
        <div class="footer__body ">
            <div class="footer__logo">
                <a href="/" class="logo "> <img src="img/logo-footer.svg" alt="logo"/></a>
            </div>
            <div class="footer__body-inner">
                <div id="map" class="footer__map map">

                </div>
                <div class="footer__content">
                    <div class="row footer__row">
                        <div class="col footer__col">
                            <h4 class="footer__title">адрес</h4>
                            <p>г. Зеленоград,
                                <br>бульвар Юрьева, 25</br> </p>

                            <h4 class="footer__title">телефон</h4>
                            <div class="footer__phone phone">
                                <a href="tel:+79999999999">+7 999 999-99-99</a>
                            </div>
                        </div>
                        <div class="col footer__col">
                            <h4 class="footer__title">страницы</h4>
                            <nav class="footer__nav">
                                <ul class="footer__nav-list">
                                    <li class="footer__nav-item"><a href="" class="footer__nav-link">Каталог</a></li>
                                    <li class="footer__nav-item"><a href="delivery.html" class="footer__nav-link">Доставка
                                            и
                                            оплата</a></li>
                                    <li class="footer__nav-item"><a href="" class="footer__nav-link">Личный кабинет</a>
                                    </li>
                                    <li class="footer__nav-item"><a href="" class="footer__nav-link">Бонусы</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col footer__col">
                            <h4 class="footer__title">соц.сети</h4>
                            <div class="footer__social social">
                                <ul class="social__list">
                                    <li class="social__item"><a href="#"
                                                                class="footer__social-link social__link icon-telegram"></a>
                                    </li>
                                    <li class="social__item"><a href="#"
                                                                class="footer__social-link social__link icon-whatsapp"></a>
                                    </li>
                                    <li class="social__item"><a href="#"
                                                                class="footer__social-link social__link icon-inst"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div class="modal ">
        <div class="modal__body" data-target="first">
            <button class="quick-order__close modal-close icon-modal-close"></button>
            <div class="quick-order">
                <div class="quick-order__body">

                    <div class="quick-order__title">Быстрый заказ</div>

                    <div class="quick-order__product-item">
                        <div class="quick-order__product-item--img">
                            <img src="img/basket/basket-img.png" alt="basket-img"/>
                        </div>
                        <div class="quick-order__product-item--name">Моно букет Маттиола микс 20 шт</div>
                    </div>

                    <p class="quick-order__text">Укажите ваши данные и наш менеджер свяжется с вами для оформления
                        заказа</p>
                    <form action="#" class="quick-order__form">
                        <div class="quick-order__form-imput">
                            <input type="text" placeholder="Имя">
                        </div>
                        <div class="quick-order__form-imput">
                            <input type="tel" placeholder="+7 (XXX) XX XX XX">
                        </div>
                        <button class="btn quick-order__form-btn">Оформить заказ</button>
                    </form>

                    <div class="quick-order__bonus">
                        <a href="#" class="basket__promo icon-promo"><span>Промокод или бонусы</span></a>
                        <div class="basket__price-bonus">
                            300 <span>бонусов</span>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!--<script src="https://api-maps.yandex.ru/2.1/?apikey=ваш API-ключ&lang=ru_RU"></script>-->
<script src="js/vendors.js"></script>
<script src="js/main.js"></script>
@yield('scripts')
</body>

</html>
