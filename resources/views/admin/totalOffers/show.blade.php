@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.totalOffer.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.total-offers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.id') }}
                        </th>
                        <td>
                            {{ $totalOffer->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.name') }}
                        </th>
                        <td>
                            {{ $totalOffer->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.offers') }}
                        </th>
                        <td>
                            @foreach($totalOffer->offers as $key => $offers)
                                <span class="label label-info">{{ $offers->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.number') }}
                        </th>
                        <td>
                            {{ $totalOffer->number }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.summ') }}
                        </th>
                        <td>
                            {{ $totalOffer->summ }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.totalOffer.fields.is_constructor') }}
                        </th>
                        <td>
                            <input type="checkbox" disabled="disabled" {{ $totalOffer->is_constructor ? 'checked' : '' }}>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.total-offers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection