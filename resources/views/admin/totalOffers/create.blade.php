@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.totalOffer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.total-offers.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">{{ trans('cruds.totalOffer.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.totalOffer.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="offers">{{ trans('cruds.totalOffer.fields.offers') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('offers') ? 'is-invalid' : '' }}" name="offers[]" id="offers" multiple required>
                    @foreach($offers as $id => $offer)
                        <option value="{{ $id }}" {{ in_array($id, old('offers', [])) ? 'selected' : '' }}>{{ $offer }}</option>
                    @endforeach
                </select>
                @if($errors->has('offers'))
                    <div class="invalid-feedback">
                        {{ $errors->first('offers') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.totalOffer.fields.offers_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="number">{{ trans('cruds.totalOffer.fields.number') }}</label>
                <input class="form-control {{ $errors->has('number') ? 'is-invalid' : '' }}" type="number" name="number" id="number" value="{{ old('number', '1') }}" step="1">
                @if($errors->has('number'))
                    <div class="invalid-feedback">
                        {{ $errors->first('number') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.totalOffer.fields.number_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="summ">{{ trans('cruds.totalOffer.fields.summ') }}</label>
                <input class="form-control {{ $errors->has('summ') ? 'is-invalid' : '' }}" type="number" name="summ" id="summ" value="{{ old('summ', '') }}" step="1">
                @if($errors->has('summ'))
                    <div class="invalid-feedback">
                        {{ $errors->first('summ') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.totalOffer.fields.summ_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('is_constructor') ? 'is-invalid' : '' }}">
                    <input class="form-check-input" type="checkbox" name="is_constructor" id="is_constructor" value="1" required {{ old('is_constructor', 0) == 1 ? 'checked' : '' }}>
                    <label class="required form-check-label" for="is_constructor">{{ trans('cruds.totalOffer.fields.is_constructor') }}</label>
                </div>
                @if($errors->has('is_constructor'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_constructor') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.totalOffer.fields.is_constructor_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection