@extends('layouts.admin')
@section('content')
@can('total_offer_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.total-offers.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.totalOffer.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.totalOffer.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-TotalOffer">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.offers') }}
                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.number') }}
                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.summ') }}
                        </th>
                        <th>
                            {{ trans('cruds.totalOffer.fields.is_constructor') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($totalOffers as $key => $totalOffer)
                        <tr data-entry-id="{{ $totalOffer->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $totalOffer->id ?? '' }}
                            </td>
                            <td>
                                {{ $totalOffer->name ?? '' }}
                            </td>
                            <td>
                                @foreach($totalOffer->offers as $key => $item)
                                    <span class="badge badge-info">{{ $item->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                {{ $totalOffer->number ?? '' }}
                            </td>
                            <td>
                                {{ $totalOffer->summ ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $totalOffer->is_constructor ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $totalOffer->is_constructor ? 'checked' : '' }}>
                            </td>
                            <td>
                                @can('total_offer_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.total-offers.show', $totalOffer->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('total_offer_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.total-offers.edit', $totalOffer->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('total_offer_delete')
                                    <form action="{{ route('admin.total-offers.destroy', $totalOffer->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('total_offer_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.total-offers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-TotalOffer:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection