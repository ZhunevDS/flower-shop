@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.product.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.products.update", [$product->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.product.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $product->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.product.fields.description') }}</label>
                <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description', $product->description) }}</textarea>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="flower_care">{{ trans('cruds.product.fields.flower_care') }}</label>
                <textarea class="form-control {{ $errors->has('flower_care') ? 'is-invalid' : '' }}" name="flower_care" id="flower_care">{{ old('flower_care', $product->flower_care) }}</textarea>
                @if($errors->has('flower_care'))
                    <div class="invalid-feedback">
                        {{ $errors->first('flower_care') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.flower_care_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="categories">{{ trans('cruds.product.fields.category') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('categories') ? 'is-invalid' : '' }}" name="categories[]" id="categories" multiple>
                    @foreach($categories as $id => $category)
                        <option value="{{ $id }}" {{ (in_array($id, old('categories', [])) || $product->categories->contains($id)) ? 'selected' : '' }}>{{ $category }}</option>
                    @endforeach
                </select>
                @if($errors->has('categories'))
                    <div class="invalid-feedback">
                        {{ $errors->first('categories') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.category_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="tags">{{ trans('cruds.product.fields.tags') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('tags') ? 'is-invalid' : '' }}" name="tags[]" id="tags" multiple>
                    @foreach($tags as $id => $tag)
                        <option value="{{ $id }}" {{ (in_array($id, old('tags', [])) || $product->tags->contains($id)) ? 'selected' : '' }}>{{ $tag }}</option>
                    @endforeach
                </select>
                @if($errors->has('tags'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tags') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.tags_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="colors">{{ trans('cruds.product.fields.color') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('colors') ? 'is-invalid' : '' }}" name="colors[]" id="colors" multiple>
                    @foreach($colors as $id => $color)
                        <option value="{{ $id }}" {{ (in_array($id, old('colors', [])) || $product->colors->contains($id)) ? 'selected' : '' }}>{{ $color }}</option>
                    @endforeach
                </select>
                @if($errors->has('colors'))
                    <div class="invalid-feedback">
                        {{ $errors->first('colors') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.color_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="events">{{ trans('cruds.product.fields.event') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('events') ? 'is-invalid' : '' }}" name="events[]" id="events" multiple>
                    @foreach($events as $id => $event)
                        <option value="{{ $id }}" {{ (in_array($id, old('events', [])) || $product->events->contains($id)) ? 'selected' : '' }}>{{ $event }}</option>
                    @endforeach
                </select>
                @if($errors->has('events'))
                    <div class="invalid-feedback">
                        {{ $errors->first('events') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.event_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="size">{{ trans('cruds.product.fields.size') }}</label>
                <input class="form-control {{ $errors->has('size') ? 'is-invalid' : '' }}" type="text" name="size" id="size" value="{{ old('size', $product->size) }}">
                @if($errors->has('size'))
                    <div class="invalid-feedback">
                        {{ $errors->first('size') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.size_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="height">{{ trans('cruds.product.fields.height') }}</label>
                <input class="form-control {{ $errors->has('height') ? 'is-invalid' : '' }}" type="number" name="height" id="height" value="{{ old('height', $product->height) }}" step="1">
                @if($errors->has('height'))
                    <div class="invalid-feedback">
                        {{ $errors->first('height') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.height_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="price">{{ trans('cruds.product.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $product->price) }}" step="0.01" required>
                @if($errors->has('price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="discount_price">{{ trans('cruds.product.fields.discount_price') }}</label>
                <input class="form-control {{ $errors->has('discount_price') ? 'is-invalid' : '' }}" type="number" name="discount_price" id="discount_price" value="{{ old('discount_price', $product->discount_price) }}" step="0.01">
                @if($errors->has('discount_price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('discount_price') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.discount_price_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="in_stock">{{ trans('cruds.product.fields.in_stock') }}</label>
                <input class="form-control {{ $errors->has('in_stock') ? 'is-invalid' : '' }}" type="number" name="in_stock" id="in_stock" value="{{ old('in_stock', $product->in_stock) }}" step="1">
                @if($errors->has('in_stock'))
                    <div class="invalid-feedback">
                        {{ $errors->first('in_stock') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.in_stock_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('in_constructor') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="in_constructor" value="0">
                    <input class="form-check-input" type="checkbox" name="in_constructor" id="in_constructor" value="1" {{ $product->in_constructor || old('in_constructor', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="in_constructor">{{ trans('cruds.product.fields.in_constructor') }}</label>
                </div>
                @if($errors->has('in_constructor'))
                    <div class="invalid-feedback">
                        {{ $errors->first('in_constructor') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.in_constructor_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('bestseller') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="bestseller" value="0">
                    <input class="form-check-input" type="checkbox" name="bestseller" id="bestseller" value="1" {{ $product->bestseller || old('bestseller', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="bestseller">{{ trans('cruds.product.fields.bestseller') }}</label>
                </div>
                @if($errors->has('bestseller'))
                    <div class="invalid-feedback">
                        {{ $errors->first('bestseller') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.bestseller_helper') }}</span>
            </div>
            <div class="form-group">
                <div class="form-check {{ $errors->has('sale') ? 'is-invalid' : '' }}">
                    <input type="hidden" name="sale" value="0">
                    <input class="form-check-input" type="checkbox" name="sale" id="sale" value="1" {{ $product->sale || old('sale', 0) === 1 ? 'checked' : '' }}>
                    <label class="form-check-label" for="sale">{{ trans('cruds.product.fields.sale') }}</label>
                </div>
                @if($errors->has('sale'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sale') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.sale_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="images">{{ trans('cruds.product.fields.images') }}</label>
                <div class="needsclick dropzone {{ $errors->has('images') ? 'is-invalid' : '' }}" id="images-dropzone">
                </div>
                @if($errors->has('images'))
                    <div class="invalid-feedback">
                        {{ $errors->first('images') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.images_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    var uploadedImagesMap = {}
Dropzone.options.imagesDropzone = {
    url: '{{ route('admin.products.storeMedia') }}',
    maxFilesize: 100, // MB
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 100,
      width: 8192,
      height: 8192
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="images[]" value="' + response.name + '">')
      uploadedImagesMap[file.name] = response.name
    },
    removedfile: function (file) {
      console.log(file)
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedImagesMap[file.name]
      }
      $('form').find('input[name="images[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($product) && $product->images)
      var files = {!! json_encode($product->images) !!}
          for (var i in files) {
          var file = files[i]
          this.options.addedfile.call(this, file)
          this.options.thumbnail.call(this, file, file.preview)
          file.previewElement.classList.add('dz-complete')
          $('form').append('<input type="hidden" name="images[]" value="' + file.file_name + '">')
        }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection